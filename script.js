// Activity:



/*
	1. Create a student grading system using an arrow function. The grade categories are as follows: Failed(74 and below), Beginner (75-80), Developing (81-85), Above Average (86-90), Advanced(91-100) 

	Sample output in the console: Congratulations! Your quarterly average is 85. You have received a Developing" mark.

*/

// Code here:


const showGrades = (grade) => {
	let text;
	if (grade <= 74) {
		text = `Your grade is ${grade}. You have received a Failed mark. Study harder.`
	} else if (grade > 74 && grade <= 80) {
		text = `Your grade is ${grade}. You have received a Beginner mark. You're on the right track!`
	} else if (grade > 80 && grade <= 85) {
		text = `Your grade is ${grade}. You have received a Developing mark. Almost there, keep learning!`
	} else if (grade > 85 && grade <= 90) {
		text = `Your grade is ${grade}. You have received a Average mark. Keep it up!`
	} else if (grade > 90 && grade <= 100) {
		text = `Your grade is ${grade}. You have received a Average mark. Superb, here is your badge!`
	}

	return text;
};

console.log(showGrades(81));
/*
	2. Create an odd-even checker that will check which numbers from 1-300 are odd and which are even,

	Sample output in the console: 
		1 - odd
		2 - even
		3 - odd
		4 - even
		5 - odd
		etc.
*/

// Code here:

const checkOddEven =  function () {
	for (let i=1; i<=300; i++) {
		if (i % 2 == 0) {
			console.log('even');
		} else {
			console.log('odd');
		}
	}
}
checkOddEven();

/*
	3. Create a an object named ""hero"" and input the details using promp(). Here are the details needed: heroName, origin, description, skills(object which will contain 3 uinique skills). Convert hero JS object to JSON data format and log the output in the console.

	Sample output in the console:
		{
		        "heroName": "Aldous",
		        "origin: "Minoan Empire,
		        "description: "A guard of the Minos Labyrinth who kept his pledge even after the kingdom's fall.,
		        "skills": {
		                "skill1": "Soul Steal",
		                |"Skill2": "Explosion",
		                "Skill3": "Chase Fate"
		        }
		}
*/


const hero = {
	heroName: prompt('Hero Name'),
	origin: prompt('Origin'),
	description: prompt('Description'),
	skills: {
		skill1: prompt('Skill #1'),
		skill2: prompt('Skill #2'),
		skill3: prompt('Skill #3')
	}
}

console.log(JSON.stringify(hero));






















